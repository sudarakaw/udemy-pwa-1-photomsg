// HTTP server using Express.js

const
  DB = `${__dirname}/db.json`,

  express = require('express'),
  http = require('http'),
  socketio = require('socket.io'),
  fs = require('fs'),

  // Create HTTP server
  app = express(),
  server = http.Server(app),

  // Create web socket server
  io = socketio(server),

  // Read server messages from disk
  msgData = fs.readFileSync(DB).toString(),
  messages = msgData ? JSON.parse(msgData) : []


// Listen for new socket client connection
io.on('connection', socket => {
  // Send all messages to connecting client
  socket.emit('all_messages', messages)

  // Listen for new messages
  socket.on('add_message', message => {
    // Add to messages
    messages.unshift(message)

    // Broadcast new message to all connected clients
    socket.broadcast.emit('new_message', message)

    // Persist to disk
    fs.writeFileSync(DB, JSON.stringify(messages))
  })
})


// Server static files from client application.
app.use(express.static(`${__dirname}/../client`))

// Server static files from Node modules.
app.use('/modules', express.static(`${__dirname}/../../node_modules`))

// Start the server
server.listen(5000, '127.0.0.1', () => console.log(`Photo Message running on http://${server.address().address}:${server.address().port}`))
