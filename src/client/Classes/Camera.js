// Camera Class

class Camera {
  constructor(video_node) {
    this.video_node = video_node
  }

  // Camera feed (viewfinder) on
  switch_on() {
    // Get camera media stream and set on player <video>
    navigator.mediaDevices.getUserMedia({
      'video': {
        'width': 600,
        'height': 600
      },
      'audio': false
    })
      .then(stream => this.stream = this.video_node.srcObject = stream)
      .catch(console.error)
  }

  // Camera feed (viewfinder) off
  switch_off() {
    // Pause video node
    this.video_node.pause()

    // Stop media stream
    this.stream.getTracks()[0].stop()
  }

  // Capture photo from camera stream
  take_photo() {
    let
      // Create a <canvas> element to render the photo
      canvas = document.createElement('canvas')

    // Set canvas dimensions same as video stream
    canvas.setAttribute('width', 600)
    canvas.setAttribute('height', 600)

    let
      // Get canvas context
      ctx = canvas.getContext('2d')

    // Render the image onto canvas
    ctx.drawImage(this.video_node, 0, 0, canvas.width, canvas.height)

    // Get canvas image as a data uri
    this.photo = ctx.canvas.toDataURL()

    // Destroy canvas
    ctx = null
    canvas = null

    return this.photo
  }
}
