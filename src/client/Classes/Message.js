class Message {
  constructor() {
    this.messages = []

    // Connect to socket server
    this.socket = io()

    // Handle connection error
    this.socket.once('connect_error', () => {
      // Notify main.js via event
      window.dispatchEvent(new Event('messages_error'))
    })

    // Listen for all messages from server (sent on connect)
    this.socket.on('all_messages', messages => {
      // Update local messages
      this.messages = messages

      // Notify client via event
      window.dispatchEvent(new Event('message_ready'))
    })

    // Listen for new messages from server
    this.socket.on('new_message', message => {
      // Add to local messages
      this.messages.unshift(message)

      // Notify client via custom event
      window.dispatchEvent(new CustomEvent('message_received', { 'detail': message }))
    })
  }

  // Get all messages
  get all() {
    return this.messages
  }

  // Add a new message
  add(photo, caption) {
    const
      // Create message object
      msg = {photo, caption}

    // Add to local messages
    this.messages.unshift(msg)

    // Emit to server
    this.socket.emit('add_message', msg)

    // Return formatted message object
    return msg
  }
}
